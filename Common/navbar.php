<div class="sidebar" id="mySideBar">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div id='konto'>
        <a href="?page=profile"><i class="fas fa-user-circle fa-lg"></i>Twój profil</a>
    </div>
    <div class="box" onclick="location.href='?page=mainpage';" style="cursor: pointer;"><p><i
                    class="fas fa-home"></i>Strona Główna</p></div>
    <div class="box" onclick="location.href='?page=schedule';" style="cursor: pointer;"><p><i
                    class="fas fa-calendar-alt"></i>Grafik</p></div>
    <div class="box" onclick="location.href='?page=messages';" style="cursor: pointer;"><p><i
                    class="fas fa-envelope"></i>Wiadomości</p></div>
    <div class="box" onclick="location.href='?page=startworkers';" style="cursor: pointer;"><p><i
                    class="fas fa-users"></i>Pracownicy</p></div>
    <div class="box" onclick="location.href='?page=inv';" style="cursor: pointer;"><p><i
                    class="fas fa-folder"></i>Faktury</p></div>
    <div class="box" onclick="location.href='?page=charts';" style="cursor: pointer;"><p><i
                    class="fas fa-chart-line"></i>Zestawienia</p></div>
    <div class="box" onclick="location.href='?page=startorders';" style="cursor: pointer;"><p><i
                    class="fas fa-pen"></i>Zamówienia</p></div>
    <div class="box" onclick="location.href='?page=admin';" style="cursor: pointer;"><p><i
                    class="fas fa-user-cog"></i>Panel admina</p></div>
    <div class="box" onclick="location.href='?page=logout';" style="cursor: pointer;"><p><i
                    class="fas fa-sign-out-alt"></i>Wyloguj</p></div>

</div>

