PROJEKT APLIKACJI INTERNETOWEJ LIL SHOP 

Aplikacja przeznaczona jest dla właścicieli niedużych sklepów. Jej zadaniem jest gromadzenie wszystkich potrzebnych informacji w jednym miejscu, a co za tym idzie ułatwienie i przyśpieszenie pracy. Aplikacja jest bardzo przejrzysta oraz łatwa w obsłudze. Kierownik sklepu po zarejestrowaniu się do niej ma dostęp do wszystkich niezbędnych informacji i może:

•	składać zamówienia do konkretnych firm i wysyłać je mailowo;
•	szybko i wygodnie tworzyć i zmieniać grafik dla poszczególnych pracowników;
•	mieć wgląd w roczne oraz miesięczne zestawienia zysków i kosztów; 
•	szybko kontaktować się ze swoimi pracownikami poprzez moduł WIADOMOŚCI
•	za pomocą modułu FAKTURY mieć szybki i wygodny dostęp do faktur (obecnych i archiwalnych).


ERD:
![noimage](Public/img/erd.png)