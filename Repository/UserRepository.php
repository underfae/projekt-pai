<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//Worker.php';
require_once __DIR__.'//..//Models//Position.php';
require_once __DIR__.'//..//Models//Invoice.php';
require_once __DIR__.'//..//Models//Order.php';

class UserRepository extends Repository
{

    //====================================USERS==========================================

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users INNER JOIN roles WHERE users.id_role=roles.id_role AND email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user == false) {
            return null;
        }
        return new User(
            $user['email'],
            $user['password'],
            $user['name_surname'],
            $user['id_user'],
            $user['name'],
            $user['path_to_pic'],
            $user['birth_date'],
            $user['pesel'],
            $user['phone_number'],
            $user['id_number'],
            $user['city'],
            $user['street'],
            $user['post_code'],
            $user['house_number'],
            $user['apart_number']

        );
    }


    public function makeUser(string $name_surname, string $email, string $password)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  USERS ( name_surname, password, email) 
            VALUES (:name_surname, :password, :email)
            ');
        $stmt->bindParam(':name_surname', $name_surname, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getUsers(): array {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users INNER JOIN roles WHERE users.id_role=roles.id_role AND email != :email;
        ');
        $stmt->bindParam(':email', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $users;
    }

    public function updatePathToPic($path_to_pic, $email){

        $stmt = $this->database->connect()->prepare('UPDATE users set path_to_pic = :path_to_pic WHERE email= :email 
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':path_to_pic', $path_to_pic, PDO::PARAM_STR);
        $stmt->execute();

    }

    public function updatePassword($email, $password){

        $stmt = $this->database->connect()->prepare('UPDATE users set password = :password WHERE email= :email 
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->execute();

    }

    public function changeData($email, $birth_date, $pesel, $phone_number, $id_number, $city,$street,$house_number,$apart_number,$post_code){
        $stmt = $this->database->connect()->prepare('UPDATE users set birth_date= :birth_date, pesel= :pesel, phone_number=:phone_number, id_number=:id_number, city = :city, street=:street, house_number=:house_number,
                                                                apart_number=:apart_number, post_code=:post_code WHERE email= :email 
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':birth_date', $birth_date, PDO::PARAM_STR);
        $stmt->bindParam(':pesel', $pesel, PDO::PARAM_STR);
        $stmt->bindParam(':phone_number', $phone_number, PDO::PARAM_STR);
        $stmt->bindParam(':id_number', $id_number, PDO::PARAM_STR);
        $stmt->bindParam(':city', $city, PDO::PARAM_STR);
        $stmt->bindParam(':street', $street, PDO::PARAM_STR);
        $stmt->bindParam(':house_number', $house_number, PDO::PARAM_STR);
        $stmt->bindParam(':apart_number', $apart_number, PDO::PARAM_STR);
        $stmt->bindParam(':post_code', $post_code, PDO::PARAM_STR);
        $stmt->execute();
    }

    //=================================PRACOWNICY=========================================================

    public function getWorker(string $email): ?Worker
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM workers WHERE email = :email
        ');
        $stmt->bindParam(':email', $id_worker, PDO::PARAM_STR);
        $stmt->execute();
        $worker = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($worker == false) {
            return null;
        }
        return new Worker(
            $worker['id_worker'],
            $worker['name_surname'],
            $worker['email'],
            $worker['id_position'],
            $worker['path_to_pic'],
            $worker['id_shop'],
            $worker['payment']
        );
    }
    public function getWorkers(): array {
        $stmt = $this->database->connect()->prepare('
            SELECT workers.id_worker, workers.name_surname, workers.path_to_pic, role.name FROM workers INNER JOIN role ON role.id_position = workers.id_position
        ');
        $stmt->bindParam(':name_surname', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        $workers = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $workers;
    }

    public function delete(int $id_worker): void
    {
        try {
            $stmt = $this->database->connect()->prepare('DELETE FROM workers
WHERE id_worker = :id_worker;');
            $stmt->bindParam(':id_worker', $id_worker, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e) {
            die("Nie udało się usunąć pracownika");
        }
    }

    public function makeWorker(string $name_surname, int $id_position, string $email, string $path_to_pic, int $id_shop, int $payment)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  WORKERS ( name_surname, email, id_position, path_to_pic, id_shop, payment) 
            VALUES (:name_surname, :email, :id_position, :path_to_pic, :id_shop, :payment)
            ');
        $stmt->bindParam(':name_surname', $name_surname, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':id_position', $id_position, PDO::PARAM_INT);
        $stmt->bindParam(':path_to_pic', $path_to_pic, PDO::PARAM_STR);
        $stmt->bindParam(':payment', $payment, PDO::PARAM_STR);
        $stmt->bindParam(':id_shop', $id_shop, PDO::PARAM_STR);
        $stmt->execute();
    }


    //================================================FAKTURY====================================================

    public function getInv(): array {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM company INNER JOIN invoices WHERE company.id_company = invoices.id_company order by making_date DESC;
        ');
        $stmt->bindParam(':id_invoice', $id_invoice, PDO::PARAM_STR);
        $stmt->execute();
        $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $invoices;
    }

    public function getInvoice(): ?Invoice
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM company INNER JOIN invoices WHERE company.id_company = invoices.id_company order by making_date DESC LIMIT 1
        ');
        $stmt->bindParam(':making_date', $making_date, PDO::PARAM_STR);
        $stmt->execute();
        $invoice = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($invoice == false) {
            return null;
        }

        return new Invoice(
            $invoice['id_invoice'],
            $invoice['id_company'],
            $invoice['making_date'],
            $invoice['payment_date'],
            $invoice['invoice_number'],
            $invoice['amount'],
            $invoice['name'],
            $invoice['bank_account'],
        );
    }

    //==========================================ZAMÓWIENIA=====================================================

    public function getOrder(): ?Order
    {
        $stmt = $this->database->connect()->prepare('
            SELECT  company.name, shop.shop_name, orders.making_date, orders.delivery_date, workers.name_surname
            FROM orders INNER JOIN company INNER JOIN workers INNER JOIN shop 
            WHERE company.id_company = orders.id_company AND workers.id_worker = orders.id_worker AND orders.id_shop = shop.id_shop 
            ORDER BY making_date DESC LIMIT 1
        ');
        $stmt->bindParam(':making_date', $making_date, PDO::PARAM_STR);
        $stmt->execute();
        $order = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($order == false) {
            return null;
        }

        return new Order(
            $order['name'],
            $order['shop_name'],
            $order['making_date'],
            $order['delivery_date'],
            $order['name_surname'],
        );
    }

    public function GetOrders(): array {
        $stmt = $this->database->connect()->prepare('SELECT orders.id_order, orders.making_date, orders.delivery_date, shop.shop_name, workers.name_surname, company.name 
        FROM orders INNER JOIN company INNER JOIN workers INNER JOIN shop 
        WHERE orders.id_company = company.id_company AND shop.id_shop=orders.id_shop AND workers.id_worker=orders.id_worker 
        ORDER BY orders.making_date DESC
        ');
        $stmt->bindParam(':id_order', $id_order, PDO::PARAM_STR);
        $stmt->execute();
        $orders = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $orders;
    }

    public function getProducts(){
            $stmt = $this->database->connect()->prepare('
            SELECT products.id_product, products.name, prod_ord.quantity, orders.id_company 
            FROM prod_ord INNER JOIN products INNER JOIN orders 
            WHERE products.id_product = prod_ord.id_product AND orders.id_order = prod_ord.id_order
            AND prod_ord.id_order = 1
        ');
            $stmt->bindParam(':id_invoice', $id_invoice, PDO::PARAM_STR);
            $stmt->execute();
            $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $products;
        }
    }

