<?php
require_once 'AppController.php';
require_once __DIR__ . '//..//Models//User.php';
require_once __DIR__ . '//..//Models//Worker.php';
require_once __DIR__ . '//..//Models//Order.php';
require_once __DIR__ . '//..//Repository//UserRepository.php';
require_once 'Database.php';

class UserController extends AppController
{
    public function charts()
    {
        $this->render('charts');
    }

    //==========================FAKTURY=============================
    public function indexi()
    {
        $userRepository = new UserRepository();
        $this->render('invoices', ['invoice' => $userRepository->getInvoice()]);
    }

    public function invoices()
    {
        $userRepository = new UserRepository();
        header('Content-type: application/json');
        http_response_code(200);
        $invoices = $userRepository->getInv();
        echo $invoices ? json_encode($invoices) : '';
    }

    public function messages()
    {
        $this->render('messages');
    }

    //=======================ZAMÓWIENIA===============================
    public function indexOrders()
    {
        $userRepository = new UserRepository();
        $this->render('orders', ['order' => $userRepository->getOrder()]);
    }

    public function orders()
    {
        $userRepository = new UserRepository();
        header('Content-type: application/json');
        http_response_code(200);
        $products = $userRepository->getProducts();
        echo $products ? json_encode($products) : '';
    }

    public function indexOlderOrders()
    {
        $userRepository = new UserRepository();
        $this->render('olderorders');
    }

    public function olderorders()
    {
        $userRepository = new UserRepository();
        header('Content-type: application/json');
        http_response_code(200);
        $orders = $userRepository->getOrders();
        echo $orders ? json_encode($orders) : '';
    }

    // =========================PROFIL================================
    public function profile()
    {
        $userRepository = new UserRepository();
        $this->render('profile', ['user' => $userRepository->getUser($_SESSION['id'])]);
    }

    public function change_pic()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $path_to_pic = $_POST['path_to_pic'];
            if (empty($path_to_pic)) {
                echo('Musisz podać ścieżkę do nowego zdjęcia!');
                return;
            }
            $user = $userRepository->updatePathToPic($path_to_pic, $_SESSION['id']);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=profile");
        }
    }

    public function change_data()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $birth_date = $_POST['birth_date'];
            $pesel = $_POST['pesel'];
            $phone_number = $_POST['phone_number'];
            $id_number = $_POST['id_number'];
            $city = $_POST['city'];
            $street = $_POST['street'];
            $house_number = $_POST['house_number'];
            $apart_number = $_POST['apart_number'];
            $post_code = $_POST['post_code'];
            if (empty($birth_date)) {
                echo('Musisz podać starą ub nową datę urodzenia!');
                return;
            }
            if (empty($pesel)) {
                echo('Musisz podać stary lub nowy pesel!');
                return;
            }
            if (empty($phone_number)) {
                echo('Musisz podać stary lub nowy telefon kontaktowy!');
                return;
            }
            if (empty($id_number)) {
                echo('Musisz podać stary ub nowy identyfikator!');
                return;
            }
            if (empty($city)) {
                echo('Musisz podać stare lub nowe miasto!');
                return;
            }
            if (empty($street)) {
                echo('Musisz podać starą lub nową ulicę!');
                return;
            }
            if (empty($house_number)) {
                echo('Musisz podać stary lub nowy numer domu!');
                return;
            }
            if (empty($apart_number)) {
                echo('Musisz podać stary lub nowy numer mieszkania!');
                return;
            }
            if (empty($post_code)) {
                echo('Musisz podać stary lub nowy Kod pocztowy');
                return;
            }
            $user = $userRepository->changeData($_SESSION['id'], $birth_date, $pesel, $phone_number, $id_number, $city, $street, $house_number, $apart_number, $post_code);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=profile");
        }
    }

    public function change_pass()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $password_old = $_POST['password_old'];
            $password_new = $_POST['password_new'];
            $password_new_2 = $_POST['password_new_2'];
            if (empty($password_old)) {
                echo('Musisz podać stare hasło');
                return;
            }
            if (empty($password_new)) {
                echo('Musisz podać nowe hasło');
                return;
            }
            if (empty($password_new_2)) {
                echo('Musisz powtórzyć hasło');
                return;
            }

            $user = $userRepository->getUser($_SESSION['id']);

            if(!password_verify( $password_old,$user->getPassword())) {
                echo ('Podałeś błędne stare hasło');
                return;
            }

            if ($password_new != $password_new_2) {
                echo('Podane nowe hasła nie są identyczne');
                return;
            }

            $hashed_password_new = password_hash($password_new,PASSWORD_DEFAULT);

            $user = $userRepository->updatePassword($_SESSION['id'], $hashed_password_new);

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=profile");
        }
    }

    // =============================GRAFIK===========================
    public function schedule()
    {
        $this->render('schedule');
    }

    //=========================PRACOWNICY==============================
    public function indexWorkers()
    {
        $this->render('workers');
    }

    public function workers()
    {
        $userRepository = new UserRepository();
        header('Content-type: application/json');
        http_response_code(200);
        $workers = $userRepository->getWorkers();
        echo $workers ? json_encode($workers) : '';
    }

    public function workerDelete(): void
    {
        if (!isset($_POST['id_worker'])) {
            http_response_code(404);
            return;
        }
        $userRepository = new UserRepository();
        $worker = $userRepository->getWorker((int)$_POST['id_worker']);
        $userRepository->delete((int)$_POST['id_worker']);
        http_response_code(200);
    }

    public function workerAdd()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $name_surname = $_POST['name_surname'];
            $id_position = $_POST['id_position'];
            $email = $_POST['email'];
            $path_to_pic = $_POST['path_to_pic'];
            $id_shop = $_POST['id_shop'];
            $payment = $_POST['payment'];
//            if (empty($name_surname)) {
//                echo('Imię i nazwisko jest wymagane!');
//                return;
//            }
//            if (empty($id_position)) {
//                echo('Stanowisko jest wymagane!');
//                return;
//            }
//            if (empty($email)) {
//                echo('Email jest wymagany!');
//                return;
//            }
//            if (empty($payment)) {
//                echo('Stawka godzinowa jest wymagana!');
//                return;
//            }
//            $worker = $userRepository->getWorker($email);
//            if ($worker) {
//                die("Pracownik o tym adresie email już istnieje");
//            }
            $worker = $userRepository->makeWorker($name_surname, $id_position, $email, $path_to_pic, $id_shop, $payment);
            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=startworkers");
            return;
        }
    }
}