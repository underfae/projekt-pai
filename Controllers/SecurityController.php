<?php
require_once 'AppController.php';
require_once __DIR__ . '//..//Models//User.php';
require_once __DIR__ . '//..//Repository//UserRepository.php';


class SecurityController extends AppController
{

    public function login()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('login', ['messages' => ['Użytkownik o takim mailu nie istnieje!']]);
                return;
            }
            if(!password_verify( $password,$user->getPassword())) {
                $this->render('login', ['messages' => ['Błędne hasło!']]);
                return;
            }

            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=mainpage");
            return;
        }
        $this->render('login');
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        $this->render('login', ['messages' => ['Zostałeś wylogowany!']]);
    }

    public function register(){
        $this->render('register');
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $name_surname = $_POST['name_surname'];
            $email = $_POST['email'];
            $password_1 = $_POST['password_1'];
            $password_2 = $_POST['password_2'];

            if (empty($name_surname)) {
                $this->render('register', ['messages' => ['Imię i nazwisko jest wymagane!']]);
                return;
            }

            if (empty($email)) {
                $this->render('register', ['messages' => ['Email jest wymagany!']]);
                return;
            }

            if (empty($password_1)) {
                $this->render('register', ['messages' => ['Hasło jest wymagane!']]);
                return;
            }

            if (empty($password_2)) {
                $this->render('register', ['messages' => ['Powtórz hasło!']]);
                return;
            }

            if ($password_1 != $password_2) {
                $this->render('register', ['messages' => ['Podane hasła nie są identyczne!']]);
                return;
            }

            $user = $userRepository->getUser($email);

            if ($user) {
                $this->render('register', ['messages' => ['Użytkownik o takim adresie email już istnieje!']]);
                return;
            }
            $hashed_pass = password_hash($password_1, PASSWORD_DEFAULT);
            $user = $userRepository->makeUser($name_surname, $email, $hashed_pass);

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=login");
            return;
        }
        $this->render('register');
    }

}