<?php

class Order
{
    private $name;
    private $shop_name;
    private $making_date;
    private $delivery_date;
    private $name_surname;

    public function __construct(
        string $name,
        string $shop_name,
        string $making_date,
        string $delivery_date,
        string $name_surname
    )
    {
        $this->name = $name;
        $this->shop_name = $shop_name;
        $this->making_date = $making_date;
        $this->delivery_date = $delivery_date;
        $this->name_surname = $name_surname;
    }

    public function getCompanyName(): string
    {
        return $this->name;
    }

    public function getShopName(): string
    {
        return $this->shop_name;
    }

    public function getMakingDate(): string
    {
        return $this->making_date;
    }

    public function getDeliveryDate(): string
    {
        return $this->delivery_date;
    }

    public function getWorkerName(): string
    {
        return $this->name_surname;
    }

}