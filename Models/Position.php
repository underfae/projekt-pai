<?php

class Position
{
    private $id_position;
    private $name;

    public function __construct(
        string $id_position,
        string $name
    )
    {
        $this->id_position = $id_position;
        $this->name = $name;
    }

}