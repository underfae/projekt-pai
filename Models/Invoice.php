<?php

class Invoice
{
    private $id_invoice;
    private $id_company;
    private $making_date;
    private $payment_date;
    private $invoice_number;
    private $amount;
    private $name;
    private $bank_account;

    public function __construct(
        int $id_invoice,
        int $id_company,
        string $making_date,
        string $payment_date,
        string $invoice_number,
        string $amount,
        string $name,
        string $bank_account

    )
    {
        $this->id_invoice=$id_invoice;
        $this->id_company= $id_company;
        $this->making_date= $making_date;
        $this->payment_date= $payment_date;
        $this->invoice_number= $invoice_number;
        $this->amount= $amount;
        $this->name = $name;
        $this->bank_account= $bank_account;
    }

    public function getIdInvoice()
    {
        return $this->id_invoice;
    }

    public function getIdCompany()
    {
        return $this->id_company;
    }

    public function getMakingDate()
    {
        return $this->making_date;
    }

    public function getPaymentDate()
    {
        return $this->payment_date;
    }
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBankAccount()
    {
        return $this->bank_account;
    }

}