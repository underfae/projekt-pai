<?php
require_once 'Controllers/SecurityController.php';
require_once 'Controllers/MainPageController.php';
require_once 'Controllers/UserController.php';
require_once 'Controllers/AdminController.php';

class Routing
{
    private $routes = [];

    public function __construct()

    {
        $this->routes = [
            'mainpage' => [
                'controller' => 'MainPageController',
                'action' => 'show'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'charts' => [
                'controller' => 'UserController',
                'action' => 'charts'
            ],
            'invoices' => [
                'controller' => 'UserController',
                'action' => 'invoices'
            ],
            'inv' => [
                'controller' => 'UserController',
                'action' => 'indexi'
            ],
            'messages' => [
                'controller' => 'UserController',
                'action' => 'messages'
            ],
            'startorders' => [
                'controller' => 'UserController',
                'action' => 'indexOrders'
            ],
            'orders' => [
                'controller' => 'UserController',
                'action' => 'orders'
            ],
            'indexorders' => [
                'controller' => 'UserController',
                'action' => 'indexOlderOrders'
            ],
            'olderorders' => [
                'controller' => 'UserController',
                'action' => 'olderorders'
            ],
            'profile' => [
                'controller' => 'UserController',
                'action' => 'profile'
            ],
            'change_pic' => [
                'controller' => 'UserController',
                'action' => 'change_pic'
            ],
            'change_data' => [
                'controller' => 'UserController',
                'action' => 'change_data'
            ],
            'change_pass' => [
                'controller' => 'UserController',
                'action' => 'change_pass'
            ],
            'schedule' => [
                'controller' => 'UserController',
                'action' => 'schedule'
            ],
            'startworkers' => [
                'controller' => 'UserController',
                'action' => 'indexWorkers'
            ],
            'workers' => [
                'controller' => 'UserController',
                'action' => 'workers'
            ],
            'delete_worker' => [
                'controller' => 'UserController',
                'action' => 'workerDelete'
            ],
            'add_worker' => [
                'controller' => 'UserController',
                'action' => 'workerAdd'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index'
            ],
            'users' => [
                'controller' => 'AdminController',
                'action' => 'users'
            ]

        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';
        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];

            $action = $this->routes[$page]['action'];
            $object = new $controller;
            $object->$action();
        }
    }
}