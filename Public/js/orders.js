function getProducts() {
    const apiUrl = "http://localhost:8004";
    const $list = $('.product-list');

    $.ajax({
        url : apiUrl + '/?page=orders',
        dataType : 'json'
    })
        .done((res) => {
            $list.empty();

            res.forEach(el => {
                $list.append(`<tr>
                        <td>${el.id_product}</td>
                        <td>${el.name}</td>
                        <td>${el.quantity}</td>
                        </tr>`);
            });
        });
}