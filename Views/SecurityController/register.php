<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/register.css"/>
    <title>Zarejestruj się</title>
</head>
<body>
<div class='container'>
    <div class='logo'>
            <p>WSZYSTKO W <br> JEDNYM MIEJSCU</p>
            <img src='../../Public/img/Group%201.png'>
            <img src='../../Public/img/LilShop.png'>
    </div>
    <div class='head'>
        <i class="fas fa-arrow-left"></i>
        <h1> REJESTRACJA</h1>
    </div>
    <h2>Utwórz konto</h2>
    <div class="mess">
        <?php
        if (isset($messages)) {
            foreach ($messages as $message) {
                echo $message;
            }
        }
        ?>
    </div>
    <form action="?page=register" method="POST">
            <p>Imię i nazwisko</p>
            <input name="name_surname" type="text" placeholder="Anna Nowak">
            <p>E-mail</p>
            <input name="email" type="text" placeholder="example@example.com" >
            <p>Hasło</p>
            <input name="password_1" type="password" placeholder="●●●●●●●">
            <p>Powtórz hasło</p>
            <input name="password_2" type="password" placeholder="●●●●●●●" >
            <button class='button1' type="submit">KONTYNUUJ <i class="fas fa-arrow-right fa-lg"> </i></button>
    </form>

</div>
</body>
</html>