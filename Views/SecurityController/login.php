<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/index.css"/>
    <title>LilShop</title>
</head>
<body>
<div class='container'>
    <div class='logo'>
        <img src='../../Public/img/Group%201.png'>
        <img src='../../Public/img/LilShop.png'>
    </div>

    <form action="?page=login" method="POST">
        <p>
            WITAMY!
        </p>
        <div class="messages">
            <?php
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>
        <div class="inputWithIcon">
            <input name="email" type="text" placeholder="EMAIL">
            <i class="fas fa-user" aria-hidden="true"></i>
        </div>
        <div class="inputWithIcon">
            <input name="password" type="password" placeholder="HASŁO">
            <i class="fas fa-lock" aria-hidden="true"></i>
        </div>
        <button class='button1' type="submit"><i class="fas fa-arrow-right fa-lg" style="color: rgb(73, 134, 184) "></i>
        </button>
        <button class='button2' type="button"><i class="fas fa-pen"></i><a href="?page=register">ZAREJESTRUJ SIĘ</a>  </button>
        <button class='button3' type="button"><i class="fab fa-google-plus-g"></i>ZALOGUJ SIĘ Z GOOGLE</button>
    </form>
</div>
</body>
</html>
