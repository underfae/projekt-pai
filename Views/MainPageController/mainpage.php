<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/mainpage.css"/>
    <title>Strona Główna</title>
</head>
<body>

<div class="wrapper">
    <div class="logo">
        <img class="woz" src="../../Public/img/Group%201.png" alt="Brak zdjęcia">
        <img class="napis" src="../../Public/img/LilShop.png" alt="Brak zdjęcia">
    </div>
    <div class="a">
        <button class="okr1" onclick="location.href='?page=schedule';" style="cursor: pointer;">
            <p>GRAFIK</p>
        </button>
    </div>
    <div class="b">
        <button class="okr2" onclick="location.href='?page=startorders';" style="cursor: pointer;">
            <p>ZAMÓWIENIA</p>
        </button>
    </div>
    <div class="c">
        <button class="okr3" onclick="location.href='?page=profile';" style="cursor: pointer;">
            <p>MÓJ PROFIL</p>
        </button>
    </div>
    <div class="d">
        <button class="okr4" onclick="location.href='?page=messages;" style="cursor: pointer;">
            <p>WIADOMOŚCI</p>
        </button>
    </div>
    <div class="e">
        <button class="okr5" onclick="location.href='?page=charts';" style="cursor: pointer;">
            <p>ZESTAWIENIA</p>
        </button>
    </div>
    <div class="f">
        <button class="okr6" onclick="location.href='?page=startworkers';" style="cursor: pointer;">
            <p>PRACOWNICY</p>
        </button>
    </div>
    <div class="g">
        <button class="okr7" onclick="location.href='?page=inv';" style="cursor: pointer;">
            <p>FAKTURY</p>
        </button>
    </div>
</div>
</body>
</html> 