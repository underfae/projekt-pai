<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/schedule.css"/>
    <title>Grafik</title>
</head>
<body>
<div class="wrapper">
    <?php include("Common/navbar.php") ?>
    <div class="content">
        <?php include("Common/header.php") ?>
        <link rel="Stylesheet" type="text/css" href="../../Public/css/schedule.css"/>
        <div class="section">
            <div>
                <button onclick="openNav()" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                </button>
            </div>
            <div class="navbar">
                <div class="date">
                    21.10.2019r - 27.10.2019r
                </div>
                <button class="one">EDYTUJ <i class="fas fa-pen"></i></button>
                <button class="two">WYSZUKAJ PRACOWNIKA <i class="fas fa-search"></i></button>
            </div>
            <div class="table">
                <table class="sch">
                    <tr class="days">
                        <td class="date"></td>
                        <td class="date"><p>21.10 Pn</p></td>
                        <td class="date"><p>22.10 Wt</p></td>
                        <td class="date"><p>23.10 Śr</p></td>
                        <td class="date"><p>24.10 Cz</p></td>
                        <td class="date"><p>25.10 Pt</p></td>
                        <td class="date"><p>26.10 Sb</p></td>
                        <td class="date"><p>27.10 Nd</p></td>
                    </tr>
                    <tr>
                        <td class="user"><p>Marcin Grzelak</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td><p>6:00-14:00</p></td>
                        <td class="w"><p>WOLNE</p></td>
                    </tr>
                    <tr>
                        <td class="user"><p>Marek Tomczyk</td>
                        <td class="w"><p>WOLNE</p></td>
                        <td class="w"><p>WOLNE</p></td>
                        <td><p>14:00-22:00</p></td>
                        <td><p>14:00-22:00</p></td>
                        <td><p>14:00-22:00</p></td>
                        <td><p>14:00-22:00</p></td>
                        <td><p>14:00-22:00</p></td>
                    </tr>
                    <tr>
                        <td class="user"><p>Klaudia Cyplis</p></td>
                        <td><p>10:00-18:00</p></td>
                        <td><p>10:00-18:00</p></td>
                        <td><p>10:00-18:00</p></td>
                        <td><p>10:00-18:00</p></td>
                        <td class="w"><p>WOLNE</p></td>
                        <td class="w"><p>WOLNE</p></td>
                        <td class="w"><p>WOLNE</p></td>
                    </tr>
                    <tr>
                        <td class="user"><p>Krystyna Kazia</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                        <td><p>18:00-23:00</p></td>
                    </tr>
                    <tr>
                        <td class="user"><p>Szymon Kloch</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                        <td class="u"><p>URLOP</p></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</body>
</html>