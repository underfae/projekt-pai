<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/invoices.css"/>
    <script src="../../Public/js/invoices.js"></script>
    <title>Faktury</title>
</head>
<body>
<div class="wrapper">
    <?php include("Common/navbar.php") ?>

    <div class="content">
        <?php include("Common/header.php") ?>
        <div class="section">
            <div>
                <button onclick="openNav()" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                </button>
            </div>

            <h1> OSTATNIA FAKTURA</h1>
            <div class="up">
                <div class="window">
                    <div class="left">
                        <p> Nazwa firmy:</p>
                        <p> Numer rachunku:</p>
                        <p> Termin płatności:</p>
                        <p> Konto bankowe:</p>
                    </div>
                    <div class="center">
                        <p><?=$invoice->getName()?></p>
                        <p><?=$invoice->getInvoiceNumber()?></p>
                        <p><?=$invoice->getMakingDate()?></p>
                        <p><?=$invoice->getBankAccount()?></p>
                    </div>
                    <div class="right">
                        <p>KWOTA: <?=$invoice->getAmount()?></p>
                        <hr>
                        <button>SCZEGÓŁY <i class="fas fa-angle-right"></i></button>
                    </div>
                </div>
            </div>
            <div class="table">
                <button id="invoicesb" onclick="showInvoices()" > POPRZEDNIE FAKTURY</button>
                <table class="inv">
                    <tr class="headings">
                        <td><p>POBIERZ PDF</p></td>
                        <td><p>NAZWA FIRMY</p></td>
                        <td><p>DATA WYSTAWIENIA</p></td>
                        <td><p>KWOTA</p></td>
                        <td><p>TERMIN PŁATNOŚCI</p></td>
                        <td></td>
                    </tr>
                    <tbody class="invoices-list">
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
</body>
</html>