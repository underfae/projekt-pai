<?php
if (!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}
if (!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>
<head>
    <?php include("Common/headings.php") ?>
    <script src="../../Public/js/olderorders.js"></script>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/olderorders.css"/>
    <title> Starsze zamówienia </title>
</head>
<body>
<div class="wrapper">
    <?php include("Common/navbar.php") ?>
    <div class="content">
        <?php include("Common/header.php") ?>
        <!--SEKCJA GŁÓWNA STRONY-->
        <div class="section">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Id zamówienia</th>
                        <th scope="col">Data utworzenia</th>
                        <th scope="col">Data dostarczenia</th>
                        <th scope="col">Nazwa sklepu</th>
                        <th scope="col">Imię i nazwisko zamawiającego</th>
                        <th scope="col">Nazwa dostawcy</th>
                    </tr>
                    </thead>
                    <tbody class="olderorders-list">
                    </tbody>
                </table>

                <button type="button" onclick="getAllOrders()">
                    Pobierz wszystkie zamówienia
                </button>

            </div>
        </div>
    </div>

</body>
</html>
