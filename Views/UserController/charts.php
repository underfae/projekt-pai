<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/charts.css"/>
    <title>Zestawienia</title>
</head>
<body>
<div class="wrapper">
    <?php include("Common/navbar.php") ?>
    <div class="content">
        <?php include("Common/header.php") ?>
        <div class="section">
            <div>
                <button onclick="openNav()" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                </button>
            </div>
            <div class="left">
                <div class="first">
                    <p class="z">ZYSKI</p>
                </div>
                <div class="second">
                    <form>
                        <div class="lft">
                            <p> MIESIĄC</p>
                            <input type="text" placeholder="Marzec">
                        </div>
                        <div class="rgth">
                            <p>ROK</p>
                            <input type="text" placeholder="2019">
                        </div>
                    </form>
                </div>
                <div class="third">
                    <div class="table">
                        <table>
                            <tr>
                                <td>
                                    <p>TYDZIEŃ</p>
                                </td>
                                <td>
                                    <p>CAŁKOWITY ZYSK</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p> 1</p>
                                </td>
                                <td class="z">
                                    <p>1243.01zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>2</p>
                                </td>
                                <td class="z">
                                    <p>983.12zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>3</p>
                                </td>
                                <td class="z">
                                    <p>1702.09zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>4</p>
                                </td>
                                <td class="z">
                                    <p>359.01zł</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="buttons">
                        <button>SCZEGÓŁY ZESTAWIENIA<i class="fas fa-angle-right"></i></button>
                        <button>ZESTAWIENIE ROCZNE<i class="fas fa-angle-right"></i></button>
                        <button>DRUKUJ ZESTAWIENIE<i class="fas fa-angle-right"></i></button>
                        <button>DRUKUJ WYKRES<i class="fas fa-angle-right"></i></button>
                    </div>
                </div>
                <div class="fourth">
                    <div class="photo1">
                        <img src="../../Public/img/wykres1.png">
                    </div>
                </div>
            </div>
            <div class="line">

            </div>
            <div class="right">
                <div class="first">
                    <p class="k">KOSZTY</p>
                </div>
                <div class="second">
                    <form>
                        <div class="lft">
                            <p> MIESIĄC</p>
                            <input type="text" placeholder="Marzec">
                        </div>
                        <div class="rgth">
                            <p>ROK</p>
                            <input type="text" placeholder="2019">
                        </div>
                    </form>
                </div>
                <div class="third">
                    <div class="table">
                        <table>
                            <tr>
                                <td>
                                    <p>TYDZIEŃ</p>
                                </td>
                                <td>
                                    <p>CAŁKOWITE KOSZTY</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p> 1</p>
                                </td>
                                <td class="cz">
                                    <p>150.06zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>2</p>
                                </td>
                                <td class="cz">
                                    <p>190.21zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>3</p>
                                </td>
                                <td class="cz">
                                    <p>298.18zł</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>4</p>
                                </td>
                                <td class="cz">
                                    <p>118.11zł</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="buttons">
                        <button>SCZEGÓŁY ZESTAWIENIA<i class="fas fa-angle-right"></i></button>
                        <button>ZESTAWIENIE ROCZNE<i class="fas fa-angle-right"></i></button>
                        <button>DRUKUJ ZESTAWIENIE<i class="fas fa-angle-right"></i></button>
                        <button>DRUKUJ WYKRES<i class="fas fa-angle-right"></i></button>
                    </div>
                </div>
                <div class="fourth">
                    <div class="photo2">
                        <img src="../../Public/img/wykres2.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>