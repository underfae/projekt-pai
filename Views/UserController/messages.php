<?php
if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('You are not logged in!');
}

if(!in_array('ROLE_USER', $_SESSION['role'])) {
    die('You do not have permission to watch this page!');
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("Common/headings.php") ?>
    <link rel="Stylesheet" type="text/css" href="../../Public/css/messages.css"/>
    <title>Wiadomości</title>
</head>
<body>
<div class="wrapper">
    <?php include("Common/navbar.php") ?>
    <div class="content">
        <?php include("Common/header.php") ?>
        <div class="section">
            <div>
                <button onclick="openNav()" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                </button>
            </div>
            <div class="left">
                <div class="searchbar">
                    <form>
                        <input name="search" type="text" placeholder="Wyszukaj...">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>

                </div>
                <div class=navbar>
                    <div class="add">
                        <button><i class="fas fa-plus"></i></button>
                    </div>
                    <div class="delete">
                        <button><i class="fas fa-ban"></i></button>
                    </div>
                    <div class="grid">
                        <button><i class="fas fa-th-large"></i></button>
                    </div>
                </div>
                <div class="latest">
                    <div class="one" onclick="location.href='#';">
                        <div class="photo"></div>
                        <div class="info">
                            <p class="name">Katarzyna Milczyńska</p>
                            <p class="mess">Ty: Ok</p>
                        </div>
                    </div>
                    <div class="two" onclick="location.href='#';">
                        <div class="photo"></div>
                        <div class="info">
                            <p class="name">Marcin Grzelak</p>
                            <p class="mess">Marcin: Lorem ipsum...</p>
                        </div>
                    </div>
                    <div class="three" onclick="location.href='#';">
                        <div class="photo"></div>
                        <div class="info">
                            <p class="name">Marta Janicka</p>
                            <p class="mess">Ty: Do widzenia.</p>
                        </div>
                    </div>
                    <div class="four" onclick="location.href='#';">
                        <div class="photo"></div>
                        <div class="info">
                            <p class="name">Karol Majewski</p>
                            <p class="mess">Ty: Do zobaczenia jutro.</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="right">
                <div class="top">
                    <div class="image">
                    </div>
                    <div class="name">
                        Katarzyna Milczyńska
                    </div>
                    <div class="icon">
                        <button><i class="fas fa-phone"></i></button>
                    </div>
                </div>
                <div class="messages">
                    <div class="mess1">
                        <div class="avatar">
                            <div class="profilepic">

                            </div>
                            <div class="info">
                                <p class="frst">Gabriela</p>
                                <p class="scnd">2 godziny temu</p>
                            </div>
                        </div>
                        <div class="mess">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.
                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt.</p>
                        </div>
                    </div>
                    <div class="mess2">
                        <div class="avatar">
                            <div class="profilepic"></div>
                            <div class="info">
                                <p class="frst">Katarzyna</p>
                                <p class="scnd">2 godziny temu</p>
                            </div>
                        </div>
                        <div class="mess">
                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                        </div>
                    </div>
                    <div class="mess3">
                        <div class="avatar">
                            <div class="profilepic"></div>
                            <div class="info">
                                <p class="frst">Gabriela</p>
                                <p class="scnd">2 godziny temu</p>
                            </div>
                        </div>
                        <div class="mess">
                            <p>Ok.</p>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <form>
                        <input input name="message" type="text" placeholder="Napisz coś...">
                        <button type="submit"><i class="fas fa-arrow-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

                        